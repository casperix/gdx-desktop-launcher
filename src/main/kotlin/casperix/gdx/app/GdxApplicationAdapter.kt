package casperix.gdx.app

import casperix.app.window.Cursor
import casperix.app.window.SystemCursor
import casperix.app.window.WindowWatcher
import casperix.math.axis_aligned.int32.Dimension2i
import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx

open class GdxApplicationAdapter(val setup: (WindowWatcher) -> Unit) : ApplicationListener {
	private val watcher = object : WindowWatcher {
		override val onResize = StorageSignal(Dimension2i.ZERO)
		override val onUpdate = Signal<Double>()
		override val onPostRender = EmptySignal()
		override val onRender = EmptySignal()
		override val onPreRender = EmptySignal()
		override val onCursor = StorageSignal<Cursor>(SystemCursor.DEFAULT)
		override val onExit = EmptySignal()
	}

	override fun create() {
		setup(watcher)
	}

	override fun resize(width: Int, height: Int) {
		watcher.onResize.set(Dimension2i(width, height))
	}

	override fun render() {
		val tick = Gdx.graphics.deltaTime.toDouble()
		watcher.onUpdate.set(tick)
		watcher.onPreRender.set()
		watcher.onRender.set()
		watcher.onPostRender.set()
	}

	override fun pause() {

	}

	override fun resume() {

	}

	override fun dispose() {
		watcher.onExit.set()
	}
}