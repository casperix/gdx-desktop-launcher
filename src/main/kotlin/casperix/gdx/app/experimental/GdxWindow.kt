package casperix.gdx.app.experimental

import casperix.app.surface.DesktopWindow
import casperix.app.surface.component.Display
import casperix.app.window.Cursor
import casperix.math.axis_aligned.int32.Dimension2i
import casperix.math.vector.int32.Vector2i
import com.badlogic.gdx.Gdx

class GdxWindow : DesktopWindow {
    private var vsync = true
    private var title = "gdx-window"

    override fun requestFrame() {
        Gdx.graphics.requestRendering()
    }

    override fun getPosition(): Vector2i {
        return Vector2i(
            Gdx.graphics.safeInsetLeft,
            Gdx.graphics.safeInsetTop,
        )
    }

    override fun getSize(): Dimension2i {
        return Dimension2i(
            Gdx.graphics.width,
            Gdx.graphics.height,
        )
    }

    override fun getTitle(): String {
        return title
    }

    override fun setTitle(value: String) {
        title = value
        Gdx.graphics.setTitle(title)
    }

    override fun getVerticalSync(): Boolean {
        return vsync
    }

    override fun setVerticalSync(value: Boolean) {
        vsync = value
        Gdx.graphics.setVSync(vsync)
    }

    override fun setFullScreen(value: Boolean) {
        val dm = Gdx.graphics.getDisplayMode(Gdx.graphics.monitor)
        Gdx.graphics.setFullscreenMode(dm)
    }

    override fun setSize(value: Dimension2i) {
        Gdx.graphics.setWindowedMode(value.width, value.height)
    }

    override fun getCursor(): Cursor {
        TODO("Not yet implemented")
    }

    override fun getDisplay(): Display {
        TODO("Not yet implemented")
    }

    override fun maximize() {
        TODO("Not yet implemented")
    }

    override fun minimize() {
        TODO("Not yet implemented")
    }

    override fun restore() {
        TODO("Not yet implemented")
    }

    override fun setCursor(value: Cursor) {
        TODO("Not yet implemented")
    }

    override fun setPosition(value: Vector2i) {
        TODO("Not yet implemented")
    }

}