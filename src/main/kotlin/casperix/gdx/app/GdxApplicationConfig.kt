package casperix.gdx.app

class GdxApplicationConfig(val title: String = "TestApp", val appIconPath:String? = null, val msaa: Int = 0, val depthBits:Int = 16, val stencilBits:Int = 0)